package com.agaevskihanada.mycrypt.client.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.agaevskihanada.mycrypt.client.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileTextDialogFragment extends DialogFragment{

    EditText fileEditText;
    private final static String FILE_NAME = "MyCryptSavedData.txt";
    Button saveTextButton, cancelButton;

    public FileTextDialogFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.file_dialog, null);

        fileEditText = view.findViewById(R.id.fileEditText);
        openFile();

        saveTextButton = view.findViewById(R.id.save_button);
        cancelButton = view.findViewById(R.id.cancel_button);

        saveTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = fileEditText.getText().toString();
                FileOutputStream fileOutputStream = null;
                try {
                    fileOutputStream = getActivity().openFileOutput(FILE_NAME,Context.MODE_PRIVATE );
                    fileOutputStream.write(content.getBytes());
                    Toast.makeText(getContext(), R.string.file_saved, Toast.LENGTH_SHORT).show();
                    dismiss();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    try {
                        if (fileOutputStream!=null)
                            fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        final AlertDialog dialog = builder.setView(view).setTitle(R.string.file_name).setIcon(R.drawable.file).create();

        dialog.show();
        return dialog;
    }



    private void openFile() {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = getActivity().openFileInput(FILE_NAME);
            byte[] bytes = new byte[fileInputStream.available()];
            fileInputStream.read(bytes);
            String text = new String(bytes);
            fileEditText.setText(text );
        }catch (IOException ex){
            ex.printStackTrace();
        }finally {
            try {
                if (fileInputStream!=null)
                    fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
