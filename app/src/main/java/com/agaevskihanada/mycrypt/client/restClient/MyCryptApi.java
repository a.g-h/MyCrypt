package com.agaevskihanada.mycrypt.client.restClient;

import com.agaevskihanada.mycrypt.client.restClient.domain.User;

import java.util.List;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MyCryptApi {
    @GET("/users/names")
    Call<List> getAllUsernames();

    @POST("/registration")
    @Headers("Accept:application/json")
    Call<ResponseBody> register(@Header ("Authorization") String credentials, @Body User user);

    @POST("/login")
    @Headers("Accept:application/json")
    Call<ResponseBody> login (@Header("Authorization") String credentials, @Body User user);

    @GET("/username")
    @Headers("Accept:application/json")
    Call<ResponseBody> getUserName(@Header("Authorization") String access_token);

    @GET("/users/{username}")
    @Headers("Accept:application/json")
    Call<ResponseBody> getUserPass(@Path("username") String username, @Header("Authorization") String access_token);

    @DELETE("/users/delete/{username}")
    @Headers("Accept:application/json")
    Call<ResponseBody> deleteUser(@Path("username") String username, @Header("Authorization") String access_token);

    @PUT("/users")
    @Headers("Accept:application/json")
    Call<ResponseBody> updateUser(@Body User user, @Header("Authorization") String access_token);

    @GET("/refresh")
    @Headers("Accept:application/json")
    Call<ResponseBody> refreshToken(@Header("Authorization") String refresh_token);
}

