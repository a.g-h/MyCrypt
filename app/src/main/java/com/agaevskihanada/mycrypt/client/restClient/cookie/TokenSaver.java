package com.agaevskihanada.mycrypt.client.restClient.cookie;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;

public class TokenSaver {
    public static void storeCookies(JSONObject result){
        AppCookieStore store = CookiesStoreHolder.getInstance().getAppCookieStore();
        try {
            store.add(new URI("/login"), new HttpCookie("access_token", result.getString("access_token")));
            store.add(new URI("/login"), new HttpCookie("refresh_token", result.getString("refresh_token")));
        } catch (URISyntaxException | JSONException e) {
            e.printStackTrace();
        }
    }
}
