package com.agaevskihanada.mycrypt.client.restClient.cookie;

import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.List;

public class AppCookieStore implements CookieStore, Runnable {

    private CookieStore store ;

    AppCookieStore() {
        store = new CookieManager().getCookieStore();

        Runtime.getRuntime().addShutdownHook(new Thread(this));
    }

    @Override
    public void add(URI uri, HttpCookie cookie){
        cookie.setHttpOnly(true);
        store.add(uri, cookie);
    }

    @Override
    public List<HttpCookie> get(URI uri) {
        return store.get(uri);
    }

    public String getOne(String cookieName){
        List<HttpCookie> list = getCookies();
        for (HttpCookie cookie : list) {
            if (cookie.getName().equals(cookieName)){
                return cookie.getValue();
            }
        }
        return null;
    }

    @Override
    public List<HttpCookie> getCookies() {
        return store.getCookies();
    }

    @Override
    public List<URI> getURIs() {
        return store.getURIs();
    }

    @Override
    public boolean remove(URI uri, HttpCookie cookie) {
        return store.remove(uri,cookie);
    }

    @Override
    public boolean removeAll() {
        return store.removeAll();
    }

    @Override
    public void run() {

    }
}
