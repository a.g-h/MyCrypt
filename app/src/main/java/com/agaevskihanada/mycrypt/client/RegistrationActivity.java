package com.agaevskihanada.mycrypt.client;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agaevskihanada.mycrypt.client.restClient.RestClient;
import com.agaevskihanada.mycrypt.client.restClient.cookie.TokenSaver;
import com.agaevskihanada.mycrypt.client.restClient.domain.User;
import com.agaevskihanada.mycrypt.client.security.CredentialsChecker;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.save_password_button)Button savePasswordButton;
    @BindView(R.id.username_warning_text)TextView usernameWarningText;
    @BindView(R.id.username_text)EditText userNameText;
    @BindView(R.id.new_password_text)EditText newPasswordText;
    @BindView(R.id.new_password_warning_text)TextView newPasswordWarningsText;
    @BindView(R.id.confirm_new_password_text)EditText confirmNewPasswordText;
    @BindView(R.id.confirm_new_password_warning_text)TextView confirmNewPasswordWarningText;


    private final String PREFS_REGISTRATION_SCREEN = "RegistrationScreenShowed";
    SharedPreferences preferences;
    private static final String TAG = "MyCrypt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        preferences = getSharedPreferences(TAG, MODE_PRIVATE);
    }

    @OnClick({R.id.save_password_button})
    public void onSaveButtonClicked(){
        String pass = newPasswordText.getText().toString();
        String userName = userNameText.getText().toString();
        String confirmedPass = confirmNewPasswordText.getText().toString();

        if (checkPassword(pass) & checkUserName(userName) & checkPasswordConfirmation(pass, confirmedPass)){
            User user = new User(userName,pass);
            Call<ResponseBody> result = MyCryptApp.getApi().register(Credentials.basic(user.getUsername(),user.getPassword()),user);
            result.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code()==201) {
                        preferences.edit().putBoolean(PREFS_REGISTRATION_SCREEN, true).apply();
                        Intent intent = new Intent(getApplicationContext(), PassLockActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    }else if (response.code()==409){
                        Toast.makeText(getApplicationContext(), R.string.user_already_exists, Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplicationContext(),R.string.smtn_went_wrong, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("registration", t.getMessage());
                }
            });
        }
    }

    private boolean checkPasswordConfirmation(String pass, String confirmedPass) {
        if (CredentialsChecker.checkPasswordConfirmation(pass, confirmedPass)){
            colorSetter(confirmNewPasswordText, true, confirmNewPasswordWarningText,"");
            return true;
        }else {
            colorSetter(confirmNewPasswordText, false, confirmNewPasswordWarningText, getResources().getString(R.string.confirm_new_password_warning));
            return false;
        }
    }

    private boolean checkPassword(String pass) {
        if (CredentialsChecker.checkPassword(pass)){
            colorSetter(newPasswordText, true, newPasswordWarningsText, "");
            return true;
        }else {
            colorSetter(newPasswordText, false, newPasswordWarningsText, getResources().getString(R.string.new_pass_warning));
            return false;
        }
    }

    private boolean checkUserName(String userName){

        if (CredentialsChecker.checkUserName(userName)){
            colorSetter(userNameText, true, usernameWarningText, "");
            return true;
        }else {
            colorSetter(userNameText, false, usernameWarningText, getResources().getString(R.string.username_warning));
            return false;
        }
    }

    private void colorSetter(View view, boolean isOK, TextView warningView, String warningText){
        if (isOK) {
            view.getBackground().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
        }else {
            view.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
        }
        warningView.setText(warningText);
    }

}