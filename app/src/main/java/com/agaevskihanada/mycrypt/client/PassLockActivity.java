package com.agaevskihanada.mycrypt.client;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.agaevskihanada.mycrypt.client.restClient.MyCryptApi;
import com.agaevskihanada.mycrypt.client.restClient.RestClient;
import com.agaevskihanada.mycrypt.client.restClient.cookie.AppCookieStore;
import com.agaevskihanada.mycrypt.client.restClient.cookie.CookiesStoreHolder;
import com.agaevskihanada.mycrypt.client.restClient.cookie.TokenSaver;
import com.agaevskihanada.mycrypt.client.restClient.domain.User;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnItemSelected;
import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassLockActivity extends AppCompatActivity {

    @BindView(R.id.password_input_text)
    EditText passwordInputText;
    @BindView(R.id.username_text)
    TextView usernameText;
    @BindView(R.id.timer)
    TextView timer;
    @BindView(R.id.users)
    Spinner users;


    private static final long TIME_LEFT_IN_MILLIS = 600000;

    private long timeLeftInMillis = TIME_LEFT_IN_MILLIS;
    private boolean isTimerOn;
    private long endTime;

    private boolean calledFromMainActivity;

    SharedPreferences preferences;

    private static final String PASS_LOCK_ACTIVITY = "PassLockActivity";
    private static final String TIMER_PREFS = "Timer";
    private static final String IS_TIMER_ON = "Is timer on";
    private static final String END_TIME = "End time";
    private static final String MILLIS_LEFT = "Millis left";

    private int attempts = 3;

    private static final String FORMAT = "%d min, %d sec";
    private CountDownTimer countdownTimer;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        preferences = getSharedPreferences(TIMER_PREFS, MODE_PRIVATE);
        preferences.edit().putBoolean(IS_TIMER_ON, isTimerOn)
                .putLong(END_TIME, endTime).putLong(MILLIS_LEFT, timeLeftInMillis).apply();

        if (countdownTimer != null) {
            countdownTimer.cancel();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        preferences = getSharedPreferences(TIMER_PREFS, MODE_PRIVATE);
        isTimerOn = preferences.getBoolean(IS_TIMER_ON, false);
        timeLeftInMillis = Long.valueOf(preferences.getLong(MILLIS_LEFT, 0));

        if (isTimerOn) {
            endTime = preferences.getLong(END_TIME, 0);
            timeLeftInMillis = endTime - System.currentTimeMillis();
            setTimer(timeLeftInMillis);

            if (timeLeftInMillis < 0) {
                timeLeftInMillis = 0;
                isTimerOn = false;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_lock);
        ButterKnife.bind(this);


        if ((getCallingActivity() != null) && (getCallingActivity().getClassName().equals(MainActivity.class.getName()))) {
            calledFromMainActivity = true;
        }

        setUpSpinnerElements();
    }

    private void setUpSpinnerElements() {
        Call<List> list = MyCryptApp.getApi().getAllUsernames();
        ArrayList result = new ArrayList();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, result);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        users.setAdapter(adapter);

        list.enqueue(new Callback<List>() {
            @Override
            public void onResponse(Call<List> call, Response<List> response) {
                if (response.code() == 200) {
                    result.addAll(response.body());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.register)
    public void onRegisterButtonClicked(){
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
        finish();
    }

    @OnItemSelected(R.id.users)
    public void onUsernameSelected(Spinner spinner, int position) {
        usernameText.setText(users.getItemAtPosition(position).toString());
    }

    @OnEditorAction(R.id.password_input_text)
    protected boolean passwordEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_DONE) {
            if (attempts > 0) {
                User user = new User(usernameText.getText().toString(),passwordInputText.getText().toString());
                Call<ResponseBody> result = MyCryptApp.getApi().login(Credentials.basic(user.getUsername(),user.getPassword()),user);
                result.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code()==200) {
                            try {
                                TokenSaver.storeCookies(new JSONObject(response.body().string()));
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            if (calledFromMainActivity){
                                intent.putExtra(PASS_LOCK_ACTIVITY,true);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                            startActivity(intent);
                            finish();
                        }else {
                            passwordInputText.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                            Toast.makeText(getApplicationContext(), getString(R.string.password_incorrect) + " " + --attempts
                                    + " " + getString(R.string.attemps_left), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("login", t.getMessage());
                    }
                });
            }
            if (attempts == 0) {
                isTimerOn = true;
                setTimer(TIME_LEFT_IN_MILLIS);
            }
        }
        return false;
    }


    private void setTimer(long timeLeft) {
        endTime = System.currentTimeMillis() + timeLeftInMillis;

        passwordInputText.setEnabled(false);

        countdownTimer = new CountDownTimer(timeLeft, 1000) {

            public void onTick(long millisUntilFinished) {
                timeLeftInMillis = millisUntilFinished;
                updateTimerViewText();
            }

            public void onFinish() {
                isTimerOn = false;
                passwordInputText.setEnabled(true);
            }
        }.start();
    }

    private void updateTimerViewText() {
        timer.setText("" + String.format(FORMAT,
                TimeUnit.MILLISECONDS.toMinutes(timeLeftInMillis),
                TimeUnit.MILLISECONDS.toSeconds(timeLeftInMillis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeLeftInMillis))));
    }
}
