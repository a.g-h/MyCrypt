package com.agaevskihanada.mycrypt.client;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.agaevskihanada.mycrypt.client.fragments.FileTextDialogFragment;
import com.agaevskihanada.mycrypt.client.fragments.LogoutDialogFragment;
import com.agaevskihanada.mycrypt.client.fragments.PassLockSettingFragment;
import com.agaevskihanada.mycrypt.client.restClient.CryptoListener;
import com.agaevskihanada.mycrypt.client.security.BroadcastService;
import com.agaevskihanada.mycrypt.client.security.Crypto;

import org.droidparts.widget.ClearableEditText;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;


public class MainActivity extends AppCompatActivity implements CryptoListener {

    @BindView(R.id.input) ClearableEditText input;
    @BindView(R.id.resultText) TextView resultText;

    SharedPreferences preferences;

    Crypto crypto;

    private boolean timerIsRunning;
    private boolean timerIsFinished;
    private boolean receiverIsRegistered = false;
    private boolean calledByStartingPassLock = false;
    private boolean accessedFromPassLockActivity;

    private static final String FILE_DIALOG = "File dialog";
    private static final String PASS_LOCK_ACTIVITY = "PassLockActivity";
    private static final String RESULT_TEXT = "RESULT_TEXT";
    private final static String BROADCAST_RUNNING = "isBroadcastServiceRunning";
    private final static String BROADCAST_FINISHED = "isBroadcastServiceFinished";
    private final String SALT = "Salt";

    private static final int REQUEST_ACCESS_TYPE=1;

    private final static String FILE_NAME = "MyCryptSavedData.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        preferences = getSharedPreferences("MyCrypt", MODE_PRIVATE);
        if (preferences.getString(SALT,null)==null){
            preferences.edit().putString(SALT, Crypto.genSalt()).apply();
        }

        crypto = new Crypto();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.about:
                Intent intent = new Intent(this, AboutAppActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                return true;
            case R.id.set_pass:
                PassLockSettingFragment dialog = new PassLockSettingFragment();
                dialog.show(getSupportFragmentManager(), "Set password");
                return true;
            case R.id.logout:
                LogoutDialogFragment logOutDialog = new LogoutDialogFragment();
                logOutDialog.show(getSupportFragmentManager(), "Logout");
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (receiverIsRegistered) {
            unregisterReceiver(broadcastReceiver);
            receiverIsRegistered = false;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (accessedFromPassLockActivity){
            calledByStartingPassLock = false;
        }
        if (timerIsFinished && !accessedFromPassLockActivity) {
            Intent passLockScreenIntent = new Intent(getApplicationContext(), PassLockActivity.class);
            calledByStartingPassLock = true;
            startActivityForResult(passLockScreenIntent, REQUEST_ACCESS_TYPE);
        }
        if (timerIsRunning) {
            stopService(new Intent(this, BroadcastService.class));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!calledByStartingPassLock && !accessedFromPassLockActivity) {
            registerReceiver(broadcastReceiver, new IntentFilter(BroadcastService.COUNTDOWN_BR));
            receiverIsRegistered = true;
            startService(new Intent(this, BroadcastService.class));
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            timerIsRunning = intent.getExtras().getBoolean(BROADCAST_RUNNING, false);
            timerIsFinished = intent.getExtras().getBoolean(BROADCAST_FINISHED, false);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==REQUEST_ACCESS_TYPE && resultCode==RESULT_OK){
            accessedFromPassLockActivity = data.getBooleanExtra(PASS_LOCK_ACTIVITY,false );
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(RESULT_TEXT, resultText.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        resultText.setText(savedInstanceState.getString(RESULT_TEXT));
    }


    @OnEditorAction(R.id.input)
    public boolean onEnterKeyPressed(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_DONE){
            onEncryptButtonClicked();
            return true;
        }
        return false;
    }

    @OnClick(R.id.newSaltButton)
    public void onNewSaltButtonClicked(){
        preferences.edit().putString(SALT,Crypto.genSalt()).apply();
        Toast.makeText(getApplicationContext(),R.string.salt_updated, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.encryptButton)
    public void onEncryptButtonClicked(){
        if (!input.getText().toString().matches("")) {
            crypto.encrypt(getApplicationContext(),this,input.getText().toString());
        } else {
            Toast.makeText(getApplicationContext(), R.string.write_your_text, Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.decryptButton)
    public void onDecryptButtonClicked(){
        try {
            if (!input.getText().toString().matches("")) {
                crypto.decrypt(getApplicationContext(),this,input.getText().toString());
            }else {
                Toast.makeText(getApplicationContext(), R.string.write_your_text, Toast.LENGTH_SHORT).show();
            }
        }catch (IllegalArgumentException e){
            Toast.makeText(getApplicationContext(),R.string.cannot_decrypt , Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.getTextButton)
    public void onGetTextButtonClicked() {
        if (!resultText.getText().toString().matches("")) {
            input.setText(resultText.getText().toString().replace("[DECRYPTED]:\n","").replace("[ENCRYPTED]:\n", "").trim());
            Toast.makeText(getApplicationContext(), R.string.you_can_use, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.saveTextButton)
    public void onSaveFileButtonClicked() {
        if (!resultText.getText().toString().matches("")) {
            DateFormat df = new SimpleDateFormat ("EEE, d MMM yyyy HH:mm:ss");

            String content = (df.format(Calendar.getInstance().getTime()) + "\n")
                    + (resultText.getText().toString().replace("[ENCRYPTED]:\n", ""));
            FileOutputStream outputStream;
            try {
                outputStream = openFileOutput(FILE_NAME, Context.MODE_APPEND);
                outputStream.write(content.getBytes());
                outputStream.close();
                Toast.makeText(getApplicationContext(), R.string.file_saved, Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            Toast.makeText(getApplicationContext(), R.string.nothing_to_save, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.openFileButton)
    public void onOpenFileButtonClicked() {
        FileTextDialogFragment dialog = new FileTextDialogFragment();
        dialog.setCancelable(false);
        dialog.show(getSupportFragmentManager(), FILE_DIALOG);
    }

    @Override
    public void onEncryptionResult(String result) {
        if (result.equals("401")) {
            crypto.refreshToken();
            crypto.encrypt(getApplicationContext(), this, input.getText().toString());
        } else {
            resultText.setText("[ENCRYPTED]:\n" + result + "\n");
        }
    }

    @Override
    public void onDecryptionResult(String result) {
        if (result.equals("401")) {
            crypto.refreshToken();
            crypto.decrypt(this, this, input.getText().toString());
        } else {
            resultText.setText("[DECRYPTED]:\n" + result + "\n");
        }
    }
}


