package com.agaevskihanada.mycrypt.client.restClient;

public interface CryptoListener {
    void onEncryptionResult(String result);
    void onDecryptionResult(String result);
}
