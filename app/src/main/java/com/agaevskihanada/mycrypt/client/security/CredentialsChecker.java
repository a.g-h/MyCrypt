package com.agaevskihanada.mycrypt.client.security;

import android.graphics.Color;
import android.graphics.PorterDuff;

import com.agaevskihanada.mycrypt.client.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CredentialsChecker {

    public static boolean checkPassword(String password){
        final String regex = "^.*(?=.{8,120})(?=.*[A-Z])(?=.*\\d).*$";
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }

    public static boolean checkUserName(String userName){

        final String regex = "^.*(?=.{5,15})(?=.*[a-zA-Z0-9]).*$";
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(userName);

        return matcher.matches();
    }

    public static boolean checkPasswordConfirmation(String password, String passwordConf){
        return !password.equals("") && !passwordConf.equals("") && password.equals(passwordConf);
    }
}

