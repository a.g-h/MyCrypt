package com.agaevskihanada.mycrypt.client.security;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.agaevskihanada.mycrypt.client.MyCryptApp;
import com.agaevskihanada.mycrypt.client.R;
import com.agaevskihanada.mycrypt.client.restClient.CryptoListener;
import com.agaevskihanada.mycrypt.client.restClient.RestClient;
import com.agaevskihanada.mycrypt.client.restClient.cookie.CookiesStoreHolder;
import com.agaevskihanada.mycrypt.client.restClient.cookie.TokenSaver;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class Crypto {

    private static SharedPreferences preferences;

    public void refreshToken() {
        String refresh_token = CookiesStoreHolder.getInstance().getAppCookieStore().getOne("refresh_token");
        Call<ResponseBody> result = MyCryptApp.getApi().refreshToken("Bearer " + refresh_token);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code()==200){
                    try {
                        String json = response.body().string();
                        TokenSaver.storeCookies(new JSONObject(json));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("refreshToken", t.getMessage());
            }
        });

    }

    public void encrypt(Context context, CryptoListener service, String textToEncrypt)      {
        String access_token = CookiesStoreHolder.getInstance().getAppCookieStore().getOne("access_token");
        Call<ResponseBody> result = MyCryptApp.getApi().getUserName("Bearer " + access_token);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code()==200){
                        Call<ResponseBody> result;
                        String username;
                        username = response.body().string();

                        result = MyCryptApp.getApi().getUserPass(username, "Bearer " + access_token);

                        result.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.code()==200){
                                    try {
                                        String password = response.body().string();

                                        preferences = context.getSharedPreferences("MyCrypt", MODE_PRIVATE);
                                        String salt = preferences.getString("Salt", null);

                                        TextEncryptor encryptor = Encryptors.text(password, salt);
                                        service.onEncryptionResult(encryptor.encrypt(textToEncrypt));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.d("getPassword", t.getMessage());
                            }
                        });
                    }else if (response.code()==401){
                        service.onEncryptionResult("401");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("getUsername", t.getMessage());
            }
        });
    }

    public void decrypt(Context context, CryptoListener service, String textToDecrypt)  {
        String access_token = CookiesStoreHolder.getInstance().getAppCookieStore().getOne("access_token");
        Call<ResponseBody> result = MyCryptApp.getApi().getUserName("Bearer " + access_token);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code()==200){
                        Call<ResponseBody> result;
                        String username;
                        username = response.body().string();

                        result = MyCryptApp.getApi().getUserPass(username, "Bearer " + access_token);

                        result.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.code()==200){
                                    try {
                                        String password = response.body().string();
                                        preferences = context.getSharedPreferences("MyCrypt", MODE_PRIVATE);
                                        String salt = preferences.getString("Salt", null);
                                        TextEncryptor decryptor = Encryptors.text(password, salt);
                                        service.onDecryptionResult(decryptor.decrypt(textToDecrypt));
                                    } catch (IllegalArgumentException e) {
                                        Toast.makeText(context,R.string.cannot_decrypt, Toast.LENGTH_SHORT).show();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.d("getPassword", t.getMessage());
                            }
                        });
                    }else if (response.code()==401){
                        service.onDecryptionResult("401");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("getUsername", t.getMessage());
            }
        });
    }

    public static String genSalt(){
        return KeyGenerators.string().generateKey();
    }


}
