package com.agaevskihanada.mycrypt.client.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.agaevskihanada.mycrypt.client.MyCryptApp;
import com.agaevskihanada.mycrypt.client.PassLockActivity;
import com.agaevskihanada.mycrypt.client.R;
import com.agaevskihanada.mycrypt.client.restClient.RestClient;
import com.agaevskihanada.mycrypt.client.restClient.cookie.CookiesStoreHolder;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoutDialogFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.logout_dialog, null);

        CheckBox checkBox = view.findViewById(R.id.checkDeleteAccount);

        return builder.setView(view).setTitle(R.string.exit)
                .setNegativeButton(R.string.no, null).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (checkBox.isChecked()) {
                            String access_token = CookiesStoreHolder.getInstance().getAppCookieStore().getOne("access_token");
                            Call<ResponseBody> result = MyCryptApp.getApi().getUserName("Bearer " + access_token);
                            result.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    try {
                                        if (response.code() == 200) {
                                            Call<ResponseBody> result;
                                            String username;
                                            username = response.body().string();
                                            result = MyCryptApp.getApi().deleteUser(username, "Bearer " + access_token);
                                            result.enqueue(new Callback<ResponseBody>() {
                                                @Override
                                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                                }

                                                @Override
                                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                    Log.d("logout",t.getMessage());
                                                }
                                            });
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                }
                            });
                        }
                        CookiesStoreHolder.getInstance().getAppCookieStore().removeAll();
                        Intent intent = new Intent(getActivity(), PassLockActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }).create();
    }
}
