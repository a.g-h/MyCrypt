package com.agaevskihanada.mycrypt.client.security;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class BroadcastService extends Service {

    private final static String BS_RUNNING = "isBroadcastServiceRunning";
    private final static String BS_FINISHED = "isBroadcastServiceFinished";

    private long timerDuration = 300000;

    public static final String COUNTDOWN_BR = "com.agaevskihanada.mycrypt.client.security.BroadcastService";
    Intent intent = new Intent(COUNTDOWN_BR);

    CountDownTimer countDownTimer = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        countDownTimer = new CountDownTimer(timerDuration, 1000) {
            @Override
            public void onTick(long millsUntilFinished) {
                intent.putExtra(BS_RUNNING, true);
                sendBroadcast(intent);
            }

            @Override
            public void onFinish() {
                intent.putExtra(BS_RUNNING, false);
                intent.putExtra(BS_FINISHED, true);
                sendBroadcast(intent);
            }
        }.start();
    }

    @Override
    public void onDestroy() {
        countDownTimer.cancel();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
