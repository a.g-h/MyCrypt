package com.agaevskihanada.mycrypt.client.restClient;

import android.os.AsyncTask;
import android.util.Log;

import com.agaevskihanada.mycrypt.client.MyCryptApp;
import com.agaevskihanada.mycrypt.client.restClient.cookie.AppCookieStore;
import com.agaevskihanada.mycrypt.client.restClient.cookie.CookiesStoreHolder;
import com.agaevskihanada.mycrypt.client.restClient.cookie.TokenSaver;
import com.agaevskihanada.mycrypt.client.restClient.domain.User;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RestClient {
    private static RestTemplate restTemplate = new RestTemplate();


    /**
     * now using iv4 ip for android emulator (10.0.2.2)
     */
    private static final String URI = "http://10.0.2.2:9095";

    public static String getUserPass() throws ExecutionException, InterruptedException {

        class GetUserPassTask extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                HttpHeaders headers;

                String username = null;
                ResponseEntity<String> response = null;
                try {
                    headers = createHeaders();
                    response = restTemplate.exchange(URI + "/username", HttpMethod.GET, new HttpEntity<>(headers), String.class);
                    if (response.getStatusCode()==HttpStatus.OK){
                        username = response.getBody();
                    }
                    response = restTemplate.exchange(URI + "/users/" + username, HttpMethod.GET, new HttpEntity<>(headers), String.class);
                    if (response.getStatusCode() == HttpStatus.OK) {
                        return response.getBody();
                    }
                } catch (HttpClientErrorException e){
                    if (e.getStatusCode()==HttpStatus.UNAUTHORIZED){
                        if (refreshToken()){
                            headers = createHeaders();
                            response = restTemplate.exchange(URI + "/username", HttpMethod.GET, new HttpEntity<>(headers), String.class);
                            if (response.getStatusCode()==HttpStatus.OK){
                                username = response.getBody();
                            }
                            response = restTemplate.exchange(URI + "/users/" + username, HttpMethod.GET, new HttpEntity<>(headers), String.class);
                            if (response.getStatusCode() == HttpStatus.OK) {
                                return response.getBody();
                            }
                        }
                    }
                }

                return "ERROR";
            }
        }

        GetUserPassTask task = new GetUserPassTask();
        return task.execute().get();
    }


    public static boolean update(String password) throws ExecutionException, InterruptedException {

        class UserPassUpdateTask extends AsyncTask<String, Void, Boolean> {
            @Override
            protected Boolean doInBackground(String... passwords) {
                HttpHeaders headers;
                String username;
                ResponseEntity<String> response;
                try {
                    headers = createHeaders();

                    response = restTemplate.exchange(URI + "/username", HttpMethod.GET, new HttpEntity<>(headers), String.class);
                    if (response.getStatusCode() == HttpStatus.OK) {
                        username = response.getBody();
                        User user = new User(username, passwords[0]);

                        HttpEntity <User> request = new HttpEntity<>(user, headers);
                        response = restTemplate.exchange(URI + "/users", HttpMethod.PUT, request, String.class);
                        return response.getStatusCode() == HttpStatus.OK;
                    }
                } catch (HttpClientErrorException e){
                    if (e.getStatusCode()==HttpStatus.UNAUTHORIZED){
                        if (refreshToken()){
                            headers = createHeaders();
                            response = restTemplate.exchange(URI + "/username", HttpMethod.GET, new HttpEntity<>(headers), String.class);
                            if (response.getStatusCode() == HttpStatus.OK) {
                                username = response.getBody();
                                User user = new User(username, passwords[0]);

                                HttpEntity <User> request = new HttpEntity<>(user, headers);
                                response = restTemplate.exchange(URI + "/users", HttpMethod.PUT, request, String.class);
                                return response.getStatusCode() == HttpStatus.OK;
                            }
                        }
                    }
                }
                return false;
            }
        }

        UserPassUpdateTask task = new UserPassUpdateTask();
        return task.execute(password).get();
    }

    private static HttpHeaders createHeaders(){
        String value = CookiesStoreHolder.getInstance().getAppCookieStore().getOne("access_token");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + value);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        return headers;
    }

    public static boolean checkPassword(String password) throws ExecutionException, InterruptedException {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.matches(password, RestClient.getUserPass());
    }

    private static boolean refreshToken()  {
        String value = CookiesStoreHolder.getInstance().getAppCookieStore().getOne("refresh_token");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + value);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        ResponseEntity<String> response = restTemplate.exchange(URI + "/refresh", HttpMethod.GET, new HttpEntity<>(headers), String.class);
        if (response.getStatusCode()==HttpStatus.OK) {
            JSONObject object = null;
            try {
                object = new JSONObject(response.getBody());
                TokenSaver.storeCookies(object);
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}


