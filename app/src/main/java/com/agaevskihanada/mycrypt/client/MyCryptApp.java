package com.agaevskihanada.mycrypt.client;

import android.app.Application;

import com.agaevskihanada.mycrypt.client.restClient.MyCryptApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyCryptApp extends Application {

    private static MyCryptApi api;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:9095")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(MyCryptApi.class);
    }

    public static MyCryptApi getApi(){
        return api;
    }
}
