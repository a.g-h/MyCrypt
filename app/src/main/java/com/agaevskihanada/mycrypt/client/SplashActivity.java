package com.agaevskihanada.mycrypt.client;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    SharedPreferences preferences;

    private static final String TAG = "MyCrypt";
    private static final String PREFS_REGISTRATION_SCREEN = "RegistrationScreenShowed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent passLock = new Intent(this, PassLockActivity.class);
        startActivity(passLock);
        finish();


        /*preferences = getSharedPreferences(TAG, MODE_PRIVATE);
        if (preferences.getBoolean(PREFS_REGISTRATION_SCREEN, false)){
            Intent passLock = new Intent(this, PassLockActivity.class);
            startActivity(passLock);
            finish();
        }else {
            Intent registration = new Intent(this,RegistrationActivity.class);
            startActivity(registration);
            finish();
        }*/
    }
}
