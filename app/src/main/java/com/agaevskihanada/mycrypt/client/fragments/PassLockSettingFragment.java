package com.agaevskihanada.mycrypt.client.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agaevskihanada.mycrypt.client.MyCryptApp;
import com.agaevskihanada.mycrypt.client.R;
import com.agaevskihanada.mycrypt.client.restClient.RestClient;
import com.agaevskihanada.mycrypt.client.restClient.cookie.CookiesStoreHolder;
import com.agaevskihanada.mycrypt.client.restClient.domain.User;
import com.agaevskihanada.mycrypt.client.security.CredentialsChecker;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class PassLockSettingFragment extends DialogFragment{

    @BindView(R.id.current_password_text)
    EditText currentPasswordText;
    @BindView(R.id.new_password_text)
    EditText newPasswordText;
    @BindView(R.id.confirm_new_password_text)
    EditText confirmNewPasswordText;
    @BindView(R.id.current_password_warning_text)
    TextView currentPasswordWarningText;
    @BindView(R.id.new_password_warning_text)
    TextView newPasswordWarningText;
    @BindView(R.id.confirm_new_password_warning_text)
    TextView confirmNewPasswordWarningText;

    public PassLockSettingFragment(){

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog, null);
        ButterKnife.bind(this, view);

        final AlertDialog dialog = builder.setView(view).setTitle(R.string.set_pass_lock).setIcon(R.drawable.password)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (checkPasswords()&& savePassword()){
                            clearViewsBackgrounds();
                            dialog.dismiss();
                        }else{
                            Toast.makeText(getActivity(),"Something is wrong!",Toast.LENGTH_SHORT).show();
                        }
                }
                });
            }
        });
        dialog.show();

        return dialog;
        }


    private void clearViewsBackgrounds() {
        currentPasswordText.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        confirmNewPasswordText.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        newPasswordText.getBackground().setColorFilter(Color.WHITE,PorterDuff.Mode.SRC_ATOP);
    }

    private boolean savePassword() {

        try {
            if (RestClient.update(newPasswordText.getText().toString())) {
                Toast.makeText(getActivity(), R.string.changed_successfully, Toast.LENGTH_SHORT).show();
                return true;
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        Toast.makeText(getActivity(),R.string.auth_failed,Toast.LENGTH_SHORT).show();
        return false;
    }

    private boolean checkPasswords() {
            return checkNewPassword() & compareNewPasswords() & checkCurrentPassword();

    }

    private boolean checkCurrentPassword () {
        try {
            if (RestClient.checkPassword(currentPasswordText.getText().toString())) {
                colorSetter(currentPasswordText, true, currentPasswordWarningText, "");
                return true;
            } else {
                colorSetter(currentPasswordText, false, currentPasswordWarningText, getResources().getString(R.string.current_password_incorrect_warning));
                return false;
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean compareNewPasswords (){
        if (CredentialsChecker.checkPasswordConfirmation(newPasswordText.getText().toString(),confirmNewPasswordText.getText().toString())){
            colorSetter(confirmNewPasswordText, true, confirmNewPasswordWarningText, "");
            return true;
        }else {
            colorSetter(confirmNewPasswordText, false, confirmNewPasswordWarningText, getResources().getString(R.string.confirm_new_password_warning));
            return false;
        }
    }

    private boolean checkNewPassword (){

        String pass = newPasswordText.getText().toString();

        if (CredentialsChecker.checkPassword(pass)){
            colorSetter(newPasswordText, true, newPasswordWarningText, "");
            return true;
        }else {
            colorSetter(newPasswordText, false, newPasswordWarningText, getResources().getString(R.string.new_pass_warning));
            return false;
        }
    }

    private void colorSetter(View view, boolean isOK, TextView warningView, String warningText){
        if (isOK) {
            view.getBackground().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
        }else {
            view.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
        }
        warningView.setText(warningText);
    }
}

