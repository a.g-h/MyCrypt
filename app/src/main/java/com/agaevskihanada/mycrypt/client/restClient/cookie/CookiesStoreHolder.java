package com.agaevskihanada.mycrypt.client.restClient.cookie;

public enum CookiesStoreHolder {
    INSTANCE;

    private final AppCookieStore appCookieStore;

    CookiesStoreHolder() {
        appCookieStore = new AppCookieStore();
    }

    public static CookiesStoreHolder getInstance(){
        return INSTANCE;
    }

    public AppCookieStore getAppCookieStore(){
        return appCookieStore;
    }
}
